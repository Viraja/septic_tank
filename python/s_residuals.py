# vim: set tabstop=4
# s_pH_features_on_dataset.py
#!/usr/bin/env python3
""" Analysis of residuals in linear regression."""

# Copyright (C) 2021 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 12.10.2021

############
## Imports
# built-ins
import platform

# 3rd party
from csv import reader
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from sklearn.linear_model import LinearRegression
from statsmodels.compat import lzip
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.graphics import tsaplots

# user

############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# Configure the size and position of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3), loc='lower left',
                 mode='expand', ncol=3, fontsize='small')

class BaseData:
    def __init__(self, filename, delimiter=';'):
        self.filename = filename
        self._delimiter = delimiter
        self._raw_data = self.retrieve_data()

        # Names of rows
        self._row0 = self._raw_data[0]
        self._parameter = []

        self.measurements = self.measurementsdict()

    def retrieve_data(self):
        """read the entire file into memory"""
        with open(self.filename, 'rt') as fid:
            raw_data = list(reader(fid, delimiter=self._delimiter))
        return raw_data

    def _convert(self, row_nr):
        """Convert sensor data into float or None"""
        _list = []
        for strx in range(1, len(self._raw_data)):
            x = self._raw_data[strx][row_nr]
            _list.append(x)
            param_name = self._raw_data[0][row_nr]
        return np.array(_list, dtype=float), param_name

    def measurementsdict(self):
        """ Transforms the data into a dictionary. To associate the label in
        csv file with the data"""
        name = []
        data = []
        for x in self._row0:
            _data, _name = self._convert(self._row0.index(x))
            name.append(_name)
            data.append(_data)
        return dict(zip(name, data))

def leastsquare_reg(pred, resp):
    pass
    # resp = b0 + a*pred
    # return

if __name__ == '__main__':
    PATH = '../data/Septic_Tank_vietnam.csv'
    Data = BaseData(PATH)

    pred_name = 'SRT'          # x-variable
    res_name = 'sCODcr_eff'    # y-variable

    measurement = Data.measurements
    predictor = measurement[pred_name]
    response = measurement[res_name]

    # residual analysis with the statmodels package
    model = sm.OLS(response, predictor).fit()

    # print the summary of the linear regression analysis
    print(model.summary())

    # plot residual analysis
    fig1 = plt.figure(figsize = (12, 8))
    fig1 = sm.graphics.plot_regress_exog(model, 0, fig=fig1)
    plt.title('Residuals of {} vs. {}'.format(pred_name, res_name))
    plt.show()

    # plot autocorrelation
    fig2 = tsaplots.plot_acf(response, lags=10)
    plt.title('Autocorrelatin of {} vs. {}'.format(pred_name, res_name))
    plt.show()

    # plot scatter plot of
    plt.scatter(predictor, response)
    plt.xlabel('%s' %pred_name)
    plt.ylabel('%s' %res_name)
    plt.title('Scatter plot predictor vs. response')
    plt.show()

    # plot linear regression with sklearn without intercept
    modelskl_nointer = LinearRegression(fit_intercept=False).fit(predictor.reshape((-1, 1)), response)
    modelskl_inter = LinearRegression(fit_intercept=True).fit(predictor.reshape((-1, 1)), response)

    fig, axes = plt.subplots(1, 2, figsize=(10, 3))
    xfit = np.linspace(0, 30, 5)# might need adjustment based on parameter range
    yfit_nointer = modelskl_nointer.predict(xfit[:, np.newaxis])
    yfit_inter = modelskl_inter.predict(xfit[:, np.newaxis])
    axes[0].scatter(predictor, response)
    axes[0].plot(xfit, yfit_nointer, 'black')
    axes[0].set(xlabel=pred_name,
            ylabel=res_name, title='no intercept')
    axes[1].scatter(predictor, response)
    axes[1].plot(xfit, yfit_inter, 'black')
    axes[1].set(xlabel=pred_name,
            ylabel=res_name, title='intercept')
    plt.tight_layout()
    plt.show()
    print(modelskl_nointer.score(predictor.reshape(-1, 1), response))

    # plot linear regression with sklearn without intercept
    print(modelskl_inter.score(predictor.reshape(-1, 1), response))

    # PCA not yet done
    # from sklearn.model_selection import train_test_split
    # from sklearn.pipeline import make_pipeline
    # from sklearn.decomposition import PCA
    # from sklearn.preprocessing import StandardScaler
    # X_train, X_test, y_train, y_test = train_test_split(predictor.reshape(-1, 1), response)
    # pcr = make_pipeline(StandardScaler(), PCA(n_components=1), LinearRegression())
    # pcr.fit(X_train, y_train)
    # pca = pcr.named_steps['pca']
    # fig, axes = plt.subplots(1, 2, figsize=(10, 3))
    # axes[0].scatter(pca.transform(X_test), y_test, alpha=.3, label='ground truth')
    # axes[0].scatter(pca.transform(X_test), pcr.predict(X_test), alpha=.3,
    #             label='predictions')
    # axes[0].set(xlabel='Projected data onto first PCA component',
    #         ylabel='y', title='PCR / PCA')
    # axes[0].legend()
    # plt.tight_layout()
    # plt.show()
