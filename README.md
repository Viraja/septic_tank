# Batch reactor feature based regression

## Authors
* **Mariane Schneider**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    └── python

The `data` folder is meant to store raw data and the output of scripts.


The `doc` folder contains documents used during the development of this package.


The `python` folder contains scripts implementing the analysis of the

## How to create the figures from the article?
